import { container } from 'assets/jss/admin-dashboard';

const userStyle = theme => ({
  container,
  root: {
    flexGrow: 1,
    // justifyContent: 'center',
  },
  wrapper: {
    flexGrow: 1,
    // maxWidth: '500px',
  },
  margin: {
    margin: theme.spacing.unit,
  },
  withoutLabel: {
    marginTop: theme.spacing.unit * 3,
  },
  textField: {
    flexBasis: 200,
  },
  button: {
    margin: theme.spacing.unit,
  },
});

export default userStyle;
