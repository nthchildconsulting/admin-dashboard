import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import Sidebar from 'components/Sidebar/Sidebar';
import Header from 'components/Header/Header';
import dashboardRoutes from 'routes/dashboard';
import appStyle from 'assets/jss/admin-dashboard/layouts/appStyle';

const switchRoutes = (
  <Switch>
    {dashboardRoutes.map((prop, key) => {
      if (prop.redirect) {
        return <Redirect from={prop.path} to={prop.to} key={key} />;
      }
      return <Route path={prop.path} component={prop.component} key={key} />;
    })}
  </Switch>
);

class App extends Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Header />
        <Sidebar routes={dashboardRoutes} />
        <main className={classes.content}>
          <div className={classes.toolbar} />
          {switchRoutes}
        </main>
      </div>
    );
  }
}

export default withStyles(appStyle)(App);
