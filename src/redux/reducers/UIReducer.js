import * as types from '../constants/ActionTypes';

const defaultState = {
  drawer: {
    open: false,
  },
};

const UIReducer = (state = defaultState, action) => {
  switch (action.type) {
    case types.UI_OPEN_DRAWER:
      return {
        ...state,
        drawer: {
          open: action.open,
        },
      };
    default:
      return state;
  }
};

export default UIReducer;
