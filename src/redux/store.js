import { applyMiddleware, createStore } from 'redux';
import logger from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import reducers from './reducers';

export default createStore(reducers, applyMiddleware(thunkMiddleware, logger));
