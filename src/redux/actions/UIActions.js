import * as types from '../constants/ActionTypes';

export const setOpenDrawer = open => ({
  type: types.UI_OPEN_DRAWER,
  open,
});
