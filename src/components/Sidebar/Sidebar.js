import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setOpenDrawer } from '../../redux/actions/UIActions';
import { NavLink, withRouter } from 'react-router-dom';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import Divider from '@material-ui/core/Divider';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import sidebarStyle from 'assets/jss/admin-dashboard/components/sidebarStyle';

class Sidebar extends Component {
  render() {
    const { classes, theme, open, onDrawerClose, routes } = this.props;
    const links = (
      <List>
        {routes.map((prop, key) => {
          if (prop.redirect) {
            return null;
          }
          return (
            <NavLink to={prop.path} activeClassName="active" key={key}>
              <ListItem button>
                <ListItemIcon>
                  <prop.icon />
                </ListItemIcon>
                <ListItemText primary={prop.text} />
              </ListItem>
            </NavLink>
          );
        })}
      </List>
    );
    return (
      <div>
        <Drawer
          variant="permanent"
          classes={{
            paper: classNames(
              classes.drawerPaper,
              !open && classes.drawerPaperClose
            ),
          }}
          open={open}
        >
          <div className={classes.toolbar}>
            <IconButton onClick={onDrawerClose}>
              {theme.direction === 'rtl' ? (
                <ChevronRightIcon />
              ) : (
                <ChevronLeftIcon />
              )}
            </IconButton>
          </div>
          <Divider />
          {links}
        </Drawer>
      </div>
    );
  }
}

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  onDrawerClose: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  open: state.ui.drawer.open,
});

const mapDispatchToProps = dispatch => ({
  onDrawerClose: () => dispatch(setOpenDrawer(false)),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withStyles(sidebarStyle, { withTheme: true })(Sidebar))
);
