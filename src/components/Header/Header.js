import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setOpenDrawer } from 'redux/actions/UIActions';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MenuIcon from '@material-ui/icons/Menu';
import headerStyle from 'assets/jss/admin-dashboard/components/headerStyle';

class Header extends Component {
  render() {
    const { classes, open, onDrawerOpen } = this.props;
    return (
      <div>
        <AppBar
          position="absolute"
          className={classNames(classes.appBar, open && classes.appBarShift)}
        >
          <Toolbar disableGutters={!open}>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={onDrawerOpen}
              className={classNames(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="title" color="inherit" noWrap>
              Admin Dashboard
            </Typography>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
  open: PropTypes.bool.isRequired,
  onDrawerOpen: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  open: state.ui.drawer.open,
});

const mapDispatchToProps = dispatch => ({
  onDrawerOpen: () => dispatch(setOpenDrawer(true)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(headerStyle)(Header));
