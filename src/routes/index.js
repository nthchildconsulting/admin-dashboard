import App from 'layouts/App/App';

const indexRoutes = [{ path: '/', component: App }];

export default indexRoutes;
