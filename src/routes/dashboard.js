import Dashboard from 'components/Dashboard/Dashboard';
import Settings from 'components/Settings/Settings';
import User from 'components/User/User';
import DashboardIcon from '@material-ui/icons/Dashboard';
import SettingsIcon from '@material-ui/icons/Settings';
import PersonIcon from '@material-ui/icons/Person';

const dashboardRoutes = [
  {
    path: '/dashboard',
    text: 'Dashboard',
    component: Dashboard,
    icon: DashboardIcon,
  },
  {
    path: '/settings',
    text: 'Settings',
    component: Settings,
    icon: SettingsIcon,
  },
  {
    path: '/user',
    text: 'User',
    component: User,
    icon: PersonIcon,
  },
  { redirect: true, path: '/', to: '/dashboard' },
];

export default dashboardRoutes;
